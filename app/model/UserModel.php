<?php

namespace App\Model;

use Tracy\Debugger;


class UserModel extends BaseModel
{


    /**
     * Metoda vrací seznam všech uživatelů
     */
    public function listUsers()
    {

        return $this->database->table('user')->fetchAll();
    }

    /**
     * Metoda vrací uživatele se zadaným id, pokud neexistuje vrací NoDataFound.
     * @param int  $id
     */
    public function getUser($id)
    {
        $result=$this->database->table('user')->where('id = ?',$id)->fetch();
        if($result==false)
            throw new NoDataFound;
        return $result;

    }

    /**
     * Metoda vrací vloží nového uživatele
     * @param array  $values
     * @return $id vloženého uživatele
     */
    public function insertUser($values)
    {
        if(
            strlen($values->surname)<2 ||
            preg_match("/[^A-Za-z]/",$values->firstname) ||
            preg_match("/[^A-Za-z]/",$values->surname)

        )throw new \Exception;
        return $this->database->table('user')->insert($values);
    }

    /**
     * Metoda edituje uživatele, pokud neexistuje vrací NoDataFound.
     * @param array  $values
     */
    public function updateUser($id, $values)
    {
        if(
            strlen($values->surname)<2 ||
            preg_match("/[^A-Za-z]/",$values->firstname) ||
            preg_match("/[^A-Za-z]/",$values->surname)

        )throw new \Exception;

        $result=$this->database->table('user')->where('id = ?',$id)->fetch();
        if($result==false)
            throw new NoDataFound;
        return $result->update($values);
    }

    /**
     * Metoda odebere uživatele, pokud neexistuje vrací NoDataFound.
     * @param array  $values
     */
    public function deleteUser($id)
    {
        $result=$this->database->table('user')->where('id = ?',$id)->fetch();
        if($result==false)
            throw new NoDataFound;
        return $result->delete();

    }
}
<?php

namespace App\Model;

use Tracy\Debugger;


class OrderModel extends BaseModel
{
    /**
     * Metoda vrací seznam všech uživatelů
     */
    public function listOrders()
    {
        /** TODO */
        return $this->database->table('order')->fetchAll();
    }

    /**
     * Metoda vrací uživatele se zadaným id, pokud neexistuje vrací NoDataFound.
     * @param int  $id
     */
    public function getOrder($id)
    {
        $result=$this->database->table('order')->where('id = ?',$id)->fetch();
        if($result==false)
            throw new NoDataFound;
        return $result;

    }

    /**
     * Metoda vrací vloží nový nákup
     * @param array  $values
     * @return $id vloženého nákupu
     */
    public function insertOrder($values)
    {
        if(
            strlen($values->name)<2 ||
            preg_match("/[^A-Za-z]/",$values->name)

        )throw new \Exception;

        return $this->database->table('order')->insert($values);

    }

    /**
     * Metoda edituje nákup, pokud neexistuje vrací NoDataFound.
     * @param int  $id
     * @param array  $values
     */
    public function updateOrder($id, $values)
    {
        if(
            strlen($values->name)<2 ||
            preg_match("/[^A-Za-z]/",$values->name)

        )throw new \Exception;

        $result=$this->database->table('order')->where('id = ?',$id)->fetch();
        if($result==false)
            throw new NoDataFound;
        return $result->update($values);

    }

    /**
     * Metoda odebere nákup, pokud neexistuje vrací NoDataFound.
     * @param array  $values
     */
    public function deleteOrder($id)
    {
        $result=$this->database->table('order')->where('id = ?',$id)->fetch();
        if($result==false)
            throw new NoDataFound;
        return $result->delete();

    }
}
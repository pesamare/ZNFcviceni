<?php

namespace App\Model;

use Tracy\Debugger;


class StatisticModel extends BaseModel
{

    /**
     * Metoda vrací seznam všech statistik, záznam bude mít položky name, min, max, avg a sum.
     */
    public function listStatistic()
    {
        //verze na 1 radek
        return $this->database->table('order')->select('user_id.firstname name,min(price*quantity) min,max(price*quantity) max,avg(price*quantity) avg,sum(price*quantity) sum')->group('user_id')->fetchAll();

        //zbytecne slozita verze
        /*
        $out=array();
        $users=$this->database->table('order')->group('user_id')->select('user_id')->fetchAll();
        foreach($users as $userid){
            $name=$this->database->table('user')->where('id = ?',$userid->user_id)->fetch()->firstname;

            $user=$this->database->table('order')->where('user_id = ?',$userid->user_id);
            $min=$user->min('price*quantity');
            $max=$user->max('price*quantity');
            $sum=$user->sum('price*quantity');
            $avg=$user->sum('price*quantity')/$user->count('price');
            $out[$userid->user_id]=array(
                'name'=>$name,
                'min'=>$min,
                'max'=>$max,
                'sum'=>$sum,
                'avg'=>$avg
            );
    }
        return $out;

        */
    }
  }